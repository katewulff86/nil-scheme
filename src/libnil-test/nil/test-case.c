/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L

#include <sys/wait.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <nil/test.h>

static void     global_test_case_teardown();
static void     monitor_test_case(NilTestCase *,
                                  int);
static char    *read_all_from_file(FILE *);
static NilTestCaseResult *retrieve_test_case_results(NilTestCase *,
                                                     pid_t,
                                                     int);
static NilTestCaseResult *run_test_case_in_new_process(NilTestCase *);
static void     setup_signal_handlers();
static void     signal_handler(int);
static char    *test_case_error_message(int);
static void     test_case_setup(NilTestCase *);
static void     test_case_teardown(NilTestCase *);

static NilTestCase *global_test_case;
static int      global_fd;

NilTestFunctions const nil_test_case_functions = {
	.run          = (NilTestRunFunc) nil_test_case_run,
};

NilTestCaseResult *
nil_test_case_run(NilTestCase *test_case)
{
	return run_test_case_in_new_process(test_case);
}

NilTestCaseResult *
run_test_case_in_new_process(NilTestCase *test_case)
{
	pid_t pid;
	int fildes[2];
	pipe(fildes);
	if ((pid = fork()) == 0) {
		close(fildes[0]);
		monitor_test_case(test_case, fildes[1]);
		return NULL;
	} else {
		close(fildes[1]);
		return retrieve_test_case_results(test_case,
		                                  pid,
		                                  fildes[0]);
	}
}

void
monitor_test_case(NilTestCase *test_case,
                  int fd)
{
	global_test_case = test_case;
	global_fd = fd;
	setup_signal_handlers();
	dup2(fd, 2);
	atexit(global_test_case_teardown);
	test_case_setup(test_case);
	NilTestFunc func = test_case->func;
	func(test_case->object);
	exit(0);
}

void
setup_signal_handlers()
{
	signal(SIGABRT, signal_handler);
	signal(SIGTERM, signal_handler);
}

void
signal_handler(int signal)
{
	(void) signal;
	exit(EXIT_FAILURE);
}

NilTestCaseResult *
retrieve_test_case_results(NilTestCase *test_case,
                           pid_t pid,
                           int fd)
{
	int stat;
	waitpid(pid, &stat, 0);
	NilTestCaseResult *result =
	    nil_test_case_result_make(test_case);
	if (WIFEXITED(stat)) {
		result->pass = WEXITSTATUS(stat) == 0;
	} else {
		result->pass = false;
	}
	if (!result->pass) {
		result->message = test_case_error_message(fd);
	}
	close(fd);
	return result;
}

char *
test_case_error_message(int fd)
{
	FILE *file = fdopen(fd, "r");
	char *result = read_all_from_file(file);
	fclose(file);
	return result;
}

char *
read_all_from_file(FILE *file)
{
	char *buffer = calloc(8, sizeof(*buffer));
	size_t length = 0;
	size_t capacity = 8;
	while (!feof(file)) {
		size_t bytes_to_read = capacity - length;
		ssize_t bytes_read =
		    fread(&buffer[length],
		          sizeof(*buffer),
		          bytes_to_read,
		          file);
		length += bytes_read;
		if (length == capacity) {
			capacity *= 2;
			buffer =
			    realloc(buffer,
			            capacity * sizeof(*buffer));
		}
	}
	return buffer;
}

void
global_test_case_teardown()
{
	close(global_fd);
	test_case_teardown(global_test_case);
}

void
test_case_setup(NilTestCase *test_case)
{
	NilTestFixture *fixture = test_case->fixture;
	void *object = test_case->object;
	nil_test_fixture_setup(fixture, object);
}

void
test_case_teardown(NilTestCase *test_case)
{
	NilTestFixture *fixture = test_case->fixture;
	void *object = test_case->object;
	nil_test_fixture_teardown(fixture, object);
}
