/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <nil/test.h>

static void     print_error_message(NilTestCaseResult *);

NilTestResultFunctions const nil_test_case_result_functions = {
	.report       =
	    (NilTestReportFunc) nil_test_case_result_report,
};

NilTestCaseResult *
nil_test_case_result_make(NilTestCase *test_case)
{
	NilTestCaseResult *result = calloc(1, sizeof(*result));
	result->result = (NilTestResult) {
		.functions    = &nil_test_case_result_functions,
	};
	result->test_case = test_case;
	return result;
}

int
nil_test_case_result_report(NilTestCaseResult *result)
{
	if (result->pass) {
		return 0;
	} else {
		print_error_message(result);
		return 1;
	}
}

void
print_error_message(NilTestCaseResult *result)
{
	NilTestCase *test_case = (NilTestCase *) result->test_case;
	fprintf(stderr,
	        "%s: %s\n",
	        test_case->test.name,
	        result->message);
}
