/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>

NilTestFunctions const nil_test_suite_functions = {
	.run          = (NilTestRunFunc) nil_test_suite_run,
};

NilTestSuiteResult *
nil_test_suite_run(NilTestSuite *test_suite)
{
	NilTestSuiteResult *result =
	    nil_test_suite_result_make(test_suite);
	for (size_t i = 0; i < test_suite->length; ++i) {
		NilTest *test = test_suite->tests[i];
		NilTestResult *test_result = nil_test_run(test);
		result->results[i] = test_result;
	}
	return result;
}
