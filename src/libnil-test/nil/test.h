/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define NIL_TEST_CASE(cname, name__, func__, object__, fixture__) \
NilTestCase cname = { \
	.test         = { \
		.functions    = &nil_test_case_functions, \
		.name         = name__, \
	}, \
	.fixture      = fixture__, \
	.func         = func__, \
	.object       = object__, \
}

#define NIL_TEST_SUITE(cname, name__, ...) \
void *__##cname##_tests[] = { \
	__VA_ARGS__, \
}; \
NilTestSuite cname = { \
	.test         = { \
		.functions    = &nil_test_suite_functions, \
		.name         = name__, \
	}, \
	.tests        = (NilTest **) __##cname##_tests, \
	.length       = sizeof(__##cname##_tests)/sizeof(__##cname##_tests[0]), \
}

typedef struct NilTest NilTest;
typedef struct NilTestResult NilTestResult;
typedef struct NilTestFixture NilTestFixture;
typedef struct NilTestCase NilTestCase;
typedef struct NilTestSuite NilTestSuite;
typedef struct NilTestCaseResult NilTestCaseResult;
typedef struct NilTestSuiteResult NilTestSuiteResult;

typedef NilTestResult *(*NilTestRunFunc)(NilTest *);
typedef int (*NilTestReportFunc)(NilTestResult const *);
typedef void (*NilTestSetupFunc)(void *);
typedef void (*NilTestTeardownFunc)(void *);
typedef void (*NilTestFunc)(void *);

typedef struct NilTestFunctions {
	NilTestRunFunc  run;
} NilTestFunctions;

struct NilTest {
	NilTestFunctions const *functions;
	char const     *name;
};

typedef struct NilTestResultFunctions {
	NilTestReportFunc report;
} NilTestResultFunctions;

struct NilTestResult {
	NilTestResultFunctions const *functions;
};

struct NilTestFixture {
	NilTestFixture *parent;
	NilTestSetupFunc setup;
	NilTestTeardownFunc teardown;
};

struct NilTestCase {
	NilTest         test;
	NilTestFixture *fixture;
	NilTestFunc     func;
	void           *object;
};

struct NilTestSuite {
	NilTest         test;
	NilTest       **tests;
	size_t          length;
};

struct NilTestCaseResult {
	NilTestResult   result;
	NilTestCase    *test_case;
	char const     *message;
	bool            pass;
};

struct NilTestSuiteResult {
	NilTestResult   result;
	NilTestSuite   *test_suite;
	NilTestResult **results;
	size_t          length;
};

void            nil_test_fixture_setup(NilTestFixture const *,
                                       void *);
void            nil_test_fixture_teardown(NilTestFixture const *,
                                          void *);

NilTestResult  *nil_test_run(NilTest *);
NilTestCaseResult *nil_test_case_run(NilTestCase *);
NilTestSuiteResult *nil_test_suite_run(NilTestSuite *);
int             nil_test_result_report(NilTestResult *);
int             nil_test_case_result_report(NilTestCaseResult *);
int             nil_test_suite_result_report(NilTestSuiteResult *);
NilTestCaseResult *nil_test_case_result_make(NilTestCase *);
NilTestSuiteResult *nil_test_suite_result_make(NilTestSuite *);

extern NilTestFunctions const nil_test_case_functions;
extern NilTestFunctions const nil_test_suite_functions;
extern NilTestResultFunctions const nil_test_case_result_functions;
extern NilTestResultFunctions const nil_test_suite_result_functions;
