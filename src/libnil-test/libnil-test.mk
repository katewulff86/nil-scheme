LIBNIL_TEST_A_SOURCES =                                         \
	src/libnil-test/nil/fixture.c                           \
	src/libnil-test/nil/test.c                              \
	src/libnil-test/nil/test-case.c                         \
	src/libnil-test/nil/test-case-result.c                  \
	src/libnil-test/nil/test-result.c                       \
	src/libnil-test/nil/test-suite.c                        \
	src/libnil-test/nil/test-suite-result.c

LIBNIL_TEST_A_OBJS = $(LIBNIL_TEST_A_SOURCES:%.c=%.o)
LIBNIL_TEST_A_DEPS = $(LIBNIL_TEST_A_SOURCES:%.c=deps/%.d)

libnil-test.a: $(LIBNIL_TEST_A_OBJS)
	$(AR) $(ARFLAGS) $@ $?

include $(LIBNIL_TEST_A_DEPS)
