LIBNIL_A_SOURCES =                                              \
	src/libnil/nil/class/comparable/symbol-comparable.c     \
	src/libnil/nil/class/equal/symbol-equal.c               \
	src/libnil/nil/class/hashable/symbol-hashable.c         \
	src/libnil/nil/class/ordered/symbol-ordered.c           \
	src/libnil/nil/class/write/symbol-write.c               \
	src/libnil/nil/class/comparable.c                       \
	src/libnil/nil/class/equal.c                            \
	src/libnil/nil/class/hashable.c                         \
	src/libnil/nil/class/ordered.c                          \
	src/libnil/nil/class/write.c                            \
	src/libnil/nil/type/bool.c                              \
	src/libnil/nil/type/bytevector.c                        \
	src/libnil/nil/type/char.c                              \
	src/libnil/nil/type/environment.c                       \
	src/libnil/nil/type/eof-object.c                        \
	src/libnil/nil/type/function.c                          \
	src/libnil/nil/type/hash-map.c                          \
	src/libnil/nil/type/list.c                              \
	src/libnil/nil/type/null.c                              \
	src/libnil/nil/type/pair.c                              \
	src/libnil/nil/type/string.c                            \
	src/libnil/nil/type/symbol.c                            \
	src/libnil/nil/type/thunk.c                             \
	src/libnil/nil/hash.c                                   \
	src/libnil/nil/object.c

LIBNIL_A_OBJS = $(LIBNIL_A_SOURCES:%.c=%.o)
LIBNIL_A_DEPS = $(LIBNIL_A_SOURCES:%.c=deps/%.d)

libnil.a: $(LIBNIL_A_OBJS)
	$(AR) $(ARFLAGS) $@ $?

include $(LIBNIL_A_DEPS)
