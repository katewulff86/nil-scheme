/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

typedef struct NilTerm NilTerm;
typedef struct NilType NilType;

typedef void (*NilFreeFunc)(void *);
typedef void **(*NilChildrenFunc)(void const *, size_t *);

typedef enum NilTypeFlags {
	nil_type_flags_none = 0,
	nil_type_flags_can_cycle = 1 << 1,
} NilTypeFlags;

struct NilTerm {
	size_t          rc;
	NilType const  *type;
};

struct NilType {
	NilFreeFunc     free;
	NilChildrenFunc children;
	size_t          size;
	NilTypeFlags    flags;
};

extern NilType *nil_type_meta;

void           *nil_make(NilType const *);
void           *nil_keep(void *);
void            nil_free(void *);
NilType const  *nil_type(void const *);
void          **nil_children(void const *, size_t *);
bool            nil_type_p(NilType const *, void const *);

NilFreeFunc     nil_type_free(NilType const *);
size_t          nil_type_size(NilType const *);
NilChildrenFunc nil_type_children(NilType const *);
NilTypeFlags    nil_type_flags(NilType const *);

void            nil_free_generic(void *);
void            nil_free_static(void *);

void          **nil_no_children(void const *, size_t *);

