/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <nil/hash.h>
#include <nil/object.h>
#include <nil/class/hashable.h>
#include <nil/type/symbol.h>
#include <nil/class/hashable/symbol-hashable.h>

static uint64_t nil_symbol_hash(NilHashable *,
                                NilSymbol const *);

static NilType symbol_hashable_type = {
	.free         = nil_free_static,
	.size         = sizeof(NilHashable),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilHashable nil_symbol_hashable = {
	.term         = {
		.rc           = 1,
		.type         = &symbol_hashable_type,
	},
	.hash         = (NilHashFunc) nil_symbol_hash
};

uint64_t
nil_symbol_hash(NilHashable *hashable,
                NilSymbol const *symbol)
{
	(void) hashable;
	char const *symbol_value = nil_symbol_value(symbol);
	size_t symbol_length = nil_symbol_length(symbol);
	return nil_murmur_hash(symbol_value, symbol_length, 0);
}
