/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

typedef struct NilOrdered NilOrdered;
typedef bool (*NilLessThanPFunc)(NilOrdered *,
                                 void const *,
                                 void const *);
typedef bool (*NilGreaterThanPFunc)(NilOrdered *,
                                    void const *,
                                    void const *);
typedef bool (*NilLessThanOrEqualToPFunc)(NilOrdered *,
                                          void const *,
                                          void const *);
typedef bool (*NilGreaterThanOrEqualToPFunc)(NilOrdered *,
                                             void const *,
                                             void const *);

struct NilOrdered {
	NilTerm         term;
	NilEqual       *equal;
	NilLessThanPFunc less_than_p;
	NilGreaterThanPFunc greater_than_p;
	NilLessThanOrEqualToPFunc less_than_or_equal_to_p;
	NilGreaterThanOrEqualToPFunc greater_than_or_equal_to_p;
};

bool            nil_less_than_p(NilOrdered *,
                                void const *,
                                void const *);
bool            nil_greater_than_p(NilOrdered *,
                                   void const *,
                                   void const *);
bool            nil_less_than_or_equal_to_p(NilOrdered *,
                                            void const *,
                                            void const *);
bool            nil_greater_than_or_equal_to_p(NilOrdered *,
                                               void const *,
                                               void const *);
