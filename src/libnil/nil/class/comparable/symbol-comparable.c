/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <nil/hash.h>
#include <nil/object.h>
#include <nil/type/symbol.h>
#include <nil/class/equal.h>
#include <nil/class/ordered.h>
#include <nil/class/hashable.h>
#include <nil/class/equal/symbol-equal.h>
#include <nil/class/ordered/symbol-ordered.h>
#include <nil/class/hashable/symbol-hashable.h>
#include <nil/class/comparable.h>
#include <nil/class/comparable/symbol-comparable.h>

static bool     nil_symbol_comparable_type_p(NilComparable *,
                                             void const *);

static NilType symbol_comparable_type = {
	.free         = nil_free_static,
	.size         = sizeof(NilComparable),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilComparable nil_symbol_comparable = {
	.term         = {
		.rc           = 1,
		.type         = &symbol_comparable_type,
	},
	.ordered      = &nil_symbol_ordered,
	.hashable     = &nil_symbol_hashable,
	.comparable_type_p =
	    (NilComparableTypePFunc) nil_symbol_comparable_type_p,
};

bool
nil_symbol_comparable_type_p(NilComparable *comparable,
                             void const    *term)
{
	(void) comparable;
	return nil_symbol_p(term);
}
