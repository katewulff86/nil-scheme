/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <nil/object.h>
#include <nil/class/equal.h>
#include <nil/type/symbol.h>
#include <nil/class/equal/symbol-equal.h>

static bool     symbol_equal_p(NilEqual *,
                               NilSymbol const *,
                               NilSymbol const *);
static bool     symbol_not_equal_p(NilEqual *,
                                   NilSymbol const *,
                                   NilSymbol const *);

static NilType symbol_equal_type = {
	.free         = nil_free_static,
	.size         = sizeof(NilEqual),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilEqual nil_symbol_equal = {
	.term         = {
		.rc           = 1,
		.type         = &symbol_equal_type,
	},
	.equal_p      = (NilEqualPFunc) symbol_equal_p,
	.not_equal_p  = (NilNotEqualPFunc) symbol_not_equal_p,
};

bool
symbol_equal_p(NilEqual *equal,
               NilSymbol const *symbol_a,
               NilSymbol const *symbol_b)
{
	(void) equal;
	size_t symbol_a_length = nil_symbol_length(symbol_a);
	size_t symbol_b_length = nil_symbol_length(symbol_b);
	if (symbol_a_length != symbol_b_length) {
		return false;
	}
	char const *symbol_a_value = nil_symbol_value(symbol_a);
	char const *symbol_b_value = nil_symbol_value(symbol_b);
	int cmp = strncmp(symbol_a_value,
	                  symbol_b_value,
	                  symbol_a_length);
	return cmp == 0;
}

bool
symbol_not_equal_p(NilEqual *equal,
                   NilSymbol const *symbol_a,
                   NilSymbol const *symbol_b)
{
	return !symbol_equal_p(equal, symbol_a, symbol_b);
}
