/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>

#include <nil/object.h>
#include <nil/class/equal.h>

bool
nil_equal_p(NilEqual *equal,
            void const *a,
            void const *b)
{
	NilEqualPFunc equal_p = equal->equal_p;
	return equal_p(equal, a, b);
}

bool
nil_not_equal_p(NilEqual *equal,
                void const *a,
                void const *b)
{
	NilNotEqualPFunc not_equal_p = equal->not_equal_p;
	return not_equal_p(equal, a, b);
}
