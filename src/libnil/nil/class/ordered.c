/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>

#include <nil/object.h>
#include <nil/class/equal.h>
#include <nil/class/ordered.h>

bool
nil_less_than_p(NilOrdered *ordered,
                void const *a,
                void const *b)
{
	return ordered->less_than_p(ordered, a, b);
}

bool
nil_greater_than_p(NilOrdered *ordered,
                   void const *a,
                   void const *b)
{
	return ordered->greater_than_p(ordered, a, b);
}

bool
nil_less_than_or_equal_to_p(NilOrdered *ordered,
                            void const *a,
                            void const *b)
{
	return ordered->less_than_or_equal_to_p(ordered, a, b);
}

bool
nil_greater_than_or_equal_to_p(NilOrdered *ordered,
                               void const *a,
                               void const *b)
{
	return ordered->greater_than_or_equal_to_p(ordered, a, b);
}
