/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

typedef struct NilBytevector NilBytevector;

NilBytevector  *nil_bytevector_make(uint8_t *, size_t);
NilBytevector  *nil_bytevector_make_static(uint8_t *, size_t);
bool            nil_bytevector_p(void const *);
uint8_t        *nil_bytevector_value(NilBytevector *);
size_t          nil_bytevector_length(NilBytevector *);
