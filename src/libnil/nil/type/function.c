/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>

#include <nil/object.h>
#include <nil/type/function.h>

struct NilFunction {
	NilTerm         term;
	void           *environment;
	NilFunc         func;
};

static void   **function_children(NilFunction const *,
                                  size_t            *);

static NilType const function_type = {
	.free         = nil_free_generic,
	.size         = sizeof(NilFunction),
	.children     = (NilChildrenFunc) function_children,
	.flags        = nil_type_flags_can_cycle,
};

NilFunction *
nil_function_make(void *environment, NilFunc func)
{
	NilFunction *function = nil_make(&function_type);
	function->environment = nil_keep(environment);
	function->func = func;
	return function;
}

void *
nil_function_environment(NilFunction const *function)
{
	return nil_keep(function->environment);
}

NilFunc
nil_function_func(NilFunction const *function)
{
	return function->func;
}

bool
nil_function_p(void const *term)
{
	return nil_type_p(&function_type, term);
}

void **
function_children(NilFunction const *function,
                  size_t            *num_children)
{
	void **child = malloc(sizeof(*child));
	*child = function->environment;
	*num_children = 1;
	return child;
}
