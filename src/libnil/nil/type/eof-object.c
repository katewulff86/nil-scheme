/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>

#include <nil/object.h>
#include <nil/type/eof-object.h>

struct NilEofObject {
	NilTerm         term;
};

static NilType const eof_object_type = {
	.free         = nil_free_static,
	.size         = sizeof(NilEofObject),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

static NilEofObject eof_object = {
	.term = {
		.rc           = 1,
		.type         = &eof_object_type,
	},
};

NilEofObject *
nil_eof_object_make()
{
	return nil_keep(&eof_object);
}

bool
nil_eof_object_p(void const *term)
{
	return nil_type_p(&eof_object_type, term);
}
