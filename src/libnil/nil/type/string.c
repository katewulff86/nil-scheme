/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <nil/object.h>
#include <nil/type/string.h>

struct NilString {
	NilTerm         term;
	char const     *string;
	size_t          length;
	bool            static_;
};

static void     string_free(NilString *);

static NilType const string_type = {
	.free         = (NilFreeFunc) string_free,
	.size         = sizeof(NilString),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

NilString *
nil_string_make(char const *string,
                size_t length)
{
	NilString *term = nil_make(&string_type);
	term->string = string;
	term->length = length;
	term->static_ = false;
	return term;
}

NilString *
nil_string_make_static(char const *string)
{
	size_t length = strlen(string);
	NilString *term = nil_string_make(string, length);
	term->static_ = true;
	return term;
}

bool
nil_string_p(void const *term)
{
	return nil_type_p(&string_type, term);
}

char const *
nil_string_value(NilString *term)
{
	return term->string;
}

size_t
nil_string_length(NilString *term)
{
	return term->length;
}

void
string_free(NilString *string)
{
	if (!string->static_) {
		nil_free_generic((void *) string->string);
	}
	nil_free_generic(string);
}
