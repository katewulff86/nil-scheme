/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>

#include <nil/object.h>
#include <nil/type/list.h>

struct NilList {
	NilTerm         term;
	void           *car;
	NilList        *cdr;
};

static void   **list_children(NilList const *,
                              size_t *);

static NilType const list_type = {
	.free         = nil_free_generic,
	.size         = sizeof(NilList),
	.children     = (NilChildrenFunc) list_children,
	.flags        = nil_type_flags_can_cycle,
};

NilList *
nil_list_empty()
{
	return NULL;
}

NilList *
nil_list_cons(void *car, NilList *cdr)
{
	NilList *lst = nil_make(&list_type);
	lst->car = nil_keep(car);
	lst->cdr = nil_keep(cdr);
	return lst;
}

void *
nil_list_car(NilList const *lst)
{
	return nil_keep(lst->car);
}

NilList *
nil_list_cdr(NilList const *lst)
{
	return nil_keep(lst->cdr);
}

bool
nil_list_empty_p(NilList const *lst)
{
	return lst == NULL;
}

bool
nil_list_p(void const *term)
{
	return term == NULL ||
	       nil_type_p(&list_type, term);
}

void **
list_children(NilList const *lst,
              size_t *num_children)
{
	void **children = calloc(2, sizeof(*children));
	children[0] = lst->car;
	children[1] = lst->cdr;
	*num_children = 2;
	return children;
}
