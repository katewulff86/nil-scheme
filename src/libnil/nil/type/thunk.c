/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>

#include <nil/object.h>
#include <nil/type/function.h>
#include <nil/type/thunk.h>

struct NilThunk {
	NilTerm         term;
	bool            evaluated;
	union {
		NilFunction    *function;
		void           *term;
	} u;
};

void          **thunk_children(NilThunk const *,
                               size_t         *);

static NilType const thunk_type = {
	.free         = nil_free_generic,
	.size         = sizeof(NilThunk),
	.children     = (NilChildrenFunc) thunk_children,
	.flags        = nil_type_flags_can_cycle,
};

NilThunk *
nil_thunk_make(NilFunction *function)
{
	NilThunk *thunk = nil_make(&thunk_type);
	thunk->evaluated = false;
	thunk->u.function = nil_keep(function);
	return thunk;
}

NilThunk *
nil_thunk_make_evaluated(void *term)
{
	NilThunk *thunk = nil_make(&thunk_type);
	thunk->evaluated = true;
	thunk->u.term = nil_keep(term);
	return thunk;
}

bool
nil_thunk_p(void const *term)
{
	return nil_type_p(&thunk_type, term);
}

void *
nil_thunk_evaluate(NilThunk *thunk)
{
	if (!thunk->evaluated) {
		thunk->evaluated = true;
		NilFunction *function = thunk->u.function;
		NilThunkFunc func =
		    (NilThunkFunc) nil_function_func(function);
		void *environment = nil_function_environment(function);
		void *term = func(environment);
		nil_free(environment);
		nil_free(function);
		thunk->u.term = term;
	}
	return nil_keep(thunk->u.term);
}

bool
nil_thunk_evaluated_p(NilThunk const *thunk)
{
	return thunk->evaluated;
}

void **
thunk_children(NilThunk const *thunk,
               size_t         *num_children)
{
	void **child = malloc(sizeof(*child));
	*child = thunk->u.term;
	*num_children = 1;
	return child;
}
