/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>

#include <nil/object.h>
#include <nil/type/bool.h>

struct NilBool {
	NilTerm         term;
	bool            bool_;
};

static NilType const bool_type = {
	.free         = nil_free_static,
	.size         = sizeof(NilBool),
	.children     = nil_no_children,
	.flags        = nil_type_flags_none,
};

static NilBool bool_false = {
	.term = {
		.rc           = 1,
		.type         = &bool_type,
	},
	.bool_ = false,
};

static NilBool bool_true = {
	.term = {
		.rc           = 1,
		.type         = &bool_type,
	},
	.bool_ = true,
};

NilBool        *
nil_bool_make(bool b) {
	if (b) {
		return nil_keep(&bool_true);
	} else {
		return nil_keep(&bool_false);
	}
}

bool
nil_bool_p(void const *term) {
	return nil_type_p(&bool_type, term);
}

bool
nil_bool_value(NilBool *term) {
	return term->bool_;
}
