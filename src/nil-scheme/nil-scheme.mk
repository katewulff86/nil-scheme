NIL_SCHEME_SOURCES =                                            \
	src/nil-scheme/nil-scheme.c

NIL_SCHEME_OBJS = $(NIL_SCHEME_SOURCES:%.c=%.o)
NIL_SCHEME_DEPS = $(NIL_SCHEME_SOURCES:%.c=deps/%.d)

nil-scheme: Makefile $(NIL_SCHEME_OBJS) libnil.a
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(NIL_SCHEME_OBJS) libnil.a

include $(NIL_SCHEME_DEPS)
