/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/eof-object.h>
#include "eof-object-test.h"

typedef struct TestData {
	NilEofObject   *eof_object;
} TestData;

static void     make_free_test(TestData *);
static void     eof_object_p_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(eof_object_p_test_case,
                     "eof-object?-test-case",
                     (NilTestFunc) eof_object_p_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_eof_object_suite, "eof-object-suite",
               &make_free_test_case,
               &eof_object_p_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->eof_object);
}

void
eof_object_p_test(TestData *test_data)
{
	assert(nil_eof_object_p(test_data->eof_object));
}

void
setup(TestData *test_data)
{
	test_data->eof_object = nil_eof_object_make();
}
