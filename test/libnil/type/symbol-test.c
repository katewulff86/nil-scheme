/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/symbol.h>
#include "symbol-test.h"

typedef struct TestData {
	char const     *raw_symbol;
	size_t          length;
	NilSymbol      *symbol;
} TestData;

static void     make_free_test(TestData *);
static void     symbol_p_test(TestData *);
static void     symbol_value_test(TestData *);
static void     symbol_length_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(symbol_p_test_case,
                     "symbol?-test-case",
                     (NilTestFunc) symbol_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(symbol_value_test_case,
                     "symbol-value-test-case",
                     (NilTestFunc) symbol_value_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(symbol_length_test_case,
                     "symbol-length-test-case",
                     (NilTestFunc) symbol_length_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_symbol_suite, "symbol-suite",
               &make_free_test_case,
               &symbol_p_test_case,
               &symbol_value_test_case,
               &symbol_length_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->symbol);
}

void
symbol_p_test(TestData *test_data)
{
	assert(nil_symbol_p(test_data->symbol));
}

void
symbol_value_test(TestData *test_data)
{
	assert(nil_symbol_value(test_data->symbol) ==
	       test_data->raw_symbol);
}

void
symbol_length_test(TestData *test_data)
{
	assert(nil_symbol_length(test_data->symbol) ==
	       test_data->length);
}

void
setup(TestData *test_data)
{
	test_data->raw_symbol = "symbol";
	test_data->length = strlen(test_data->raw_symbol);
	test_data->symbol =
	    nil_symbol_make_static(test_data->raw_symbol);
}
