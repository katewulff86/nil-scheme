/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/bytevector.h>
#include "bytevector-test.h"

typedef struct TestData {
	uint8_t        *vector;
	size_t          length;
	NilBytevector  *bytevector;
} TestData;

static void     make_free_static_test(TestData *);
static void     make_free_dynamic_test(void *);
static void     bytevector_p_test(TestData *);
static void     bytevector_value_test(TestData *);
static void     bytevector_length_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_static_test_case,
                     "make-free-static-test-case",
                     (NilTestFunc) make_free_static_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(make_free_dynamic_test_case,
                     "make-free-dynamic-test-case",
                     make_free_dynamic_test, NULL, NULL);
static NIL_TEST_CASE(bytevector_p_test_case,
                     "bytevector?-test-case",
                     (NilTestFunc) bytevector_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(bytevector_value_test_case,
                     "bytevector-value-test-case",
                     (NilTestFunc) bytevector_value_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(bytevector_length_test_case,
                     "bytevector-length-test-case",
                     (NilTestFunc) bytevector_length_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_bytevector_suite, "bytevector-suite",
	&make_free_static_test_case,
	&make_free_dynamic_test_case,
	&bytevector_p_test_case,
	&bytevector_value_test_case,
	&bytevector_length_test_case);

void
make_free_static_test(TestData *test_data)
{
	nil_free(test_data->bytevector);
}

void
make_free_dynamic_test(void *o)
{
	(void) o;
	uint8_t *dynamic_vector = calloc(128, sizeof(*dynamic_vector));
	NilBytevector *bytevector =
	    nil_bytevector_make(dynamic_vector, 128);
	nil_free(bytevector);
}

void
bytevector_p_test(TestData *test_data)
{
	assert(nil_bytevector_p(test_data->bytevector));
}

void
bytevector_value_test(TestData *test_data)
{
	assert(nil_bytevector_value(test_data->bytevector) ==
	       test_data->vector);
}

void
bytevector_length_test(TestData *test_data)
{
	assert(nil_bytevector_length(test_data->bytevector) ==
	       test_data->length);
}

void
setup(TestData *test_data)
{
	static uint8_t vector[] = {0, 1, 2, 3};
	test_data->vector = vector;
	test_data->length = 4;
	test_data->bytevector =
	   nil_bytevector_make_static(test_data->vector,
	                              test_data->length);

}
