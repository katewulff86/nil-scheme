/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/function.h>
#include "function-test.h"

typedef struct TestData {
	NilFunction    *function;
} TestData;

static void     make_free_test(TestData *);
static void     function_p_test(TestData *);
static void     function_environment_test(TestData *);
static void     function_func_test(TestData *);
static void     setup(TestData *);
static void     test_func(void);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(function_p_test_case,
                     "function?-test-case",
                     (NilTestFunc) function_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(function_environment_test_case,
                     "function-environment-test-case",
                     (NilTestFunc) function_environment_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(function_func_test_case,
                     "function-func-test-case",
                     (NilTestFunc) function_func_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_function_suite, "fuction-suite",
               &make_free_test_case,
               &function_p_test_case,
               &function_environment_test_case,
               &function_func_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->function);
}

void
function_p_test(TestData *test_data)
{
	assert(nil_function_p(test_data->function));
}

void
function_environment_test(TestData *test_data)
{
	assert(nil_function_environment(test_data->function) == NULL);
}

void
function_func_test(TestData *test_data)
{
	assert(nil_function_func(test_data->function) == test_func);
}

void
setup(TestData *test_data)
{
	test_data->function = nil_function_make(NULL, test_func);
}

void
test_func()
{
}
