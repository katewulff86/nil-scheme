/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/class/equal.h>
#include <nil/class/ordered.h>
#include <nil/class/hashable.h>
#include <nil/class/comparable.h>
#include <nil/class/comparable/symbol-comparable.h>
#include <nil/type/function.h>
#include <nil/type/list.h>
#include <nil/type/symbol.h>
#include <nil/type/hash-map.h>
#include "hash-map-test.h"

typedef struct TestData {
	NilHashMap     *hash_map;
	NilSymbol      *key;
	NilSymbol      *value;
	NilSymbol      *def;
} TestData;

static void     make_free_test(TestData *);
static void     hash_map_p_test(TestData *);
static void     hash_map_contains_p_test(TestData *);
static void     hash_map_empty_p_test(TestData *);
static void     hash_map_ref_test(TestData *);
static void     hash_map_ref_default_test(TestData *);
static void     hash_map_delete_test(TestData *);
static void     hash_map_pop_test(TestData *);
static void     hash_map_clear_test(TestData *);
static void     hash_map_size_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_p_test_case,
                     "hash-map?-test-case",
                     (NilTestFunc) hash_map_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_contains_p_test_case,
                     "hash-map-contains?-test-case",
                     (NilTestFunc) hash_map_contains_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_empty_p_test_case,
                     "hash-map-empty?-test-case",
                     (NilTestFunc) hash_map_empty_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_ref_test_case,
                     "hash-map-ref-test-case",
                     (NilTestFunc) hash_map_ref_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_ref_default_test_case,
                     "hash-map-ref-default-test-case",
                     (NilTestFunc) hash_map_ref_default_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_delete_test_case,
                     "hash-map-delete-test-case",
                     (NilTestFunc) hash_map_delete_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_pop_test_case,
                     "hash-map-pop-test-case",
                     (NilTestFunc) hash_map_pop_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_clear_test_case,
                     "hash-map-clear-test-case",
                     (NilTestFunc) hash_map_clear_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(hash_map_size_test_case,
                     "hash-map-size-test-case",
                     (NilTestFunc) hash_map_size_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_hash_map_suite, "hash-map-suite",
               &make_free_test_case,
               &hash_map_p_test_case,
               &hash_map_contains_p_test_case,
               &hash_map_empty_p_test_case,
               &hash_map_ref_test_case,
               &hash_map_ref_default_test_case,
               &hash_map_delete_test_case,
               &hash_map_pop_test_case,
               &hash_map_clear_test_case,
               &hash_map_size_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->hash_map);
}

void
hash_map_p_test(TestData *test_data)
{
	assert(nil_hash_map_p(test_data->hash_map));
}

void
hash_map_contains_p_test(TestData *test_data)
{
	NilHashMap *hash_map = test_data->hash_map;
	NilSymbol *key = test_data->key;
	NilSymbol *value = test_data->value;
	assert(!nil_hash_map_contains_p(hash_map, key));
	nil_hash_map_set(hash_map, key, value);
	assert(nil_hash_map_contains_p(hash_map, key));
}

void
hash_map_empty_p_test(TestData *test_data)
{
	NilHashMap *hash_map = test_data->hash_map;
	NilSymbol *key = test_data->key;
	NilSymbol *value = test_data->value;
	assert(nil_hash_map_empty_p(hash_map));
	nil_hash_map_set(hash_map, key, value);
	assert(!nil_hash_map_empty_p(hash_map));
}

void
hash_map_ref_test(TestData *test_data)
{
	NilHashMap *hash_map = test_data->hash_map;
	NilSymbol *key = test_data->key;
	NilSymbol *value = test_data->value;
	assert(nil_hash_map_ref(hash_map, key) == NULL);
	nil_hash_map_set(hash_map, key, value);
	assert(nil_hash_map_ref(hash_map, key) == value);
}

void
hash_map_ref_default_test(TestData *test_data)
{
	NilHashMap *hash_map = test_data->hash_map;
	NilSymbol *key = test_data->key;
	NilSymbol *value = test_data->value;
	NilSymbol *def = test_data->def;
	assert(nil_hash_map_ref_default(hash_map, key, def) == def);
	nil_hash_map_set(hash_map, key, value);
	assert(nil_hash_map_ref_default(hash_map, key, def) == value);
}

void
hash_map_delete_test(TestData *test_data)
{
	NilHashMap *hash_map = test_data->hash_map;
	NilSymbol *key = test_data->key;
	NilSymbol *value = test_data->value;
	assert(!nil_hash_map_delete(hash_map, key));
	nil_hash_map_set(hash_map, key, value);
	assert(!nil_hash_map_empty_p(hash_map));
	assert(nil_hash_map_delete(hash_map, key));
	assert(nil_hash_map_empty_p(hash_map));
}

void
hash_map_pop_test(TestData *test_data)
{
	NilHashMap *hash_map = test_data->hash_map;
	NilSymbol *key = test_data->key;
	NilSymbol *value = test_data->value;
	nil_hash_map_set(hash_map, key, value);
	NilSymbol *popped_key;
	NilSymbol *popped_value;
	popped_value =
	    nil_hash_map_pop(hash_map, (void **) &popped_key);
	assert(popped_key == key);
	assert(popped_value == value);
	popped_value =
	    nil_hash_map_pop(hash_map, (void **) &popped_key);
	assert(popped_key == NULL);
	assert(popped_value == NULL);
}

void
hash_map_clear_test(TestData *test_data)
{
	NilHashMap *hash_map = test_data->hash_map;
	NilSymbol *key = test_data->key;
	NilSymbol *value = test_data->value;
	NilSymbol *def = test_data->def;
	nil_hash_map_set(hash_map, key, value);
	nil_hash_map_set(hash_map, value, value);
	nil_hash_map_set(hash_map, def, value);
	nil_hash_map_clear(hash_map);
	assert(nil_hash_map_empty_p(hash_map));
}

void
hash_map_size_test(TestData *test_data)
{
	NilHashMap *hash_map = test_data->hash_map;
	NilSymbol *key = test_data->key;
	NilSymbol *value = test_data->value;
	NilSymbol *def = test_data->def;
	assert(nil_hash_map_size(hash_map) == 0);
	nil_hash_map_set(hash_map, key, value);
	assert(nil_hash_map_size(hash_map) == 1);
	nil_hash_map_set(hash_map, value, value);
	assert(nil_hash_map_size(hash_map) == 2);
	nil_hash_map_set(hash_map, def, value);
	assert(nil_hash_map_size(hash_map) == 3);
	nil_hash_map_set(hash_map, key, def);
	assert(nil_hash_map_size(hash_map) == 3);
}

void
setup(TestData *test_data)
{
	test_data->hash_map = nil_hash_map_make(&nil_symbol_comparable);
	test_data->key = nil_symbol_make_static("key");
	test_data->value = nil_symbol_make_static("value");
	test_data->def = nil_symbol_make_static("default");
}
