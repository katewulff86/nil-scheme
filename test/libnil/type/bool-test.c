/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/bool.h>
#include "bool-test.h"

typedef struct TestData {
	NilBool        *bool_false;
	NilBool        *bool_true;
} TestData;

static void     make_free_test(TestData *);
static void     bool_p_test(TestData *);
static void     bool_value_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(make_free_test_case,
                     "make-free-test-case",
                     (NilTestFunc) make_free_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(bool_p_test_case,
                     "bool?-test-case",
                     (NilTestFunc) bool_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(bool_value_test_case,
                     "bool-value-test-case",
                     (NilTestFunc) bool_value_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(type_bool_suite, "bool-suite",
               &make_free_test_case,
               &bool_p_test_case,
               &bool_value_test_case);

void
make_free_test(TestData *test_data)
{
	nil_free(test_data->bool_false);
	nil_free(test_data->bool_true);
}

void
bool_p_test(TestData *test_data)
{
	assert(nil_bool_p(test_data->bool_false));
	assert(nil_bool_p(test_data->bool_true));
}

void
bool_value_test(TestData *test_data)
{
	assert(nil_bool_value(test_data->bool_false) == false);
	assert(nil_bool_value(test_data->bool_true) == true);
}

void
setup(TestData *test_data)
{
	test_data->bool_false = nil_bool_make(false);
	test_data->bool_true = nil_bool_make(true);
}
