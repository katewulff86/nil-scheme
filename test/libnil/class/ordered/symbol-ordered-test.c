/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include <nil/object.h>
#include <nil/type/symbol.h>
#include <nil/class/equal.h>
#include <nil/class/ordered.h>
#include <nil/class/ordered/symbol-ordered.h>
#include "symbol-ordered-test.h"

typedef struct TestData {
	NilSymbol      *symbol_a;
	NilSymbol      *symbol_b;
	NilSymbol      *symbol_c;
} TestData;

static void     less_than_p_test(TestData *);
static void     greater_than_p_test(TestData *);
static void     less_than_or_equal_to_p_test(TestData *);
static void     greater_than_or_equal_to_p_test(TestData *);
static void     setup(TestData *);

static TestData test_data;
static NilTestFixture fixture = {
	.setup        = (NilTestSetupFunc) setup,
	.teardown     = NULL,
};

static NIL_TEST_CASE(less_than_p_test_case,
                     "less-than?-test-case",
                     (NilTestFunc) less_than_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(greater_than_p_test_case,
                     "greater-than?-test-case",
                     (NilTestFunc) greater_than_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(less_than_or_equal_to_p_test_case,
                     "less-than-or-equal-to?-test-case",
                     (NilTestFunc) less_than_or_equal_to_p_test,
                     &test_data,
                     &fixture);
static NIL_TEST_CASE(greater_than_or_equal_to_p_test_case,
                     "greater-than-or-equal-to?-test-case",
                     (NilTestFunc) greater_than_or_equal_to_p_test,
                     &test_data,
                     &fixture);
NIL_TEST_SUITE(class_ordered_symbol_suite, "symbol-suite",
               &less_than_p_test_case,
               &greater_than_p_test_case,
               &less_than_or_equal_to_p_test_case,
               &greater_than_or_equal_to_p_test_case);

void
less_than_p_test(TestData *test_data)
{
	assert(!nil_less_than_p(&nil_symbol_ordered,
	                        test_data->symbol_a,
	                        test_data->symbol_b));
	assert(nil_less_than_p(&nil_symbol_ordered,
	                       test_data->symbol_a,
	                       test_data->symbol_c));
	assert(!nil_less_than_p(&nil_symbol_ordered,
	                        test_data->symbol_c,
	                        test_data->symbol_a));
}

void
greater_than_p_test(TestData *test_data)
{
	assert(!nil_greater_than_p(&nil_symbol_ordered,
	                           test_data->symbol_a,
	                           test_data->symbol_b));
	assert(!nil_greater_than_p(&nil_symbol_ordered,
	                           test_data->symbol_a,
	                           test_data->symbol_c));
	assert(nil_greater_than_p(&nil_symbol_ordered,
	                          test_data->symbol_c,
	                          test_data->symbol_a));
}

void
less_than_or_equal_to_p_test(TestData *test_data)
{
	assert(nil_less_than_or_equal_to_p(&nil_symbol_ordered,
	                                   test_data->symbol_a,
	                                   test_data->symbol_b));
	assert(nil_less_than_or_equal_to_p(&nil_symbol_ordered,
	                                   test_data->symbol_a,
	                                   test_data->symbol_c));
	assert(!nil_less_than_or_equal_to_p(&nil_symbol_ordered,
	                                    test_data->symbol_c,
	                                    test_data->symbol_a));
}

void
greater_than_or_equal_to_p_test(TestData *test_data)
{
	assert(nil_greater_than_or_equal_to_p(&nil_symbol_ordered,
	                                      test_data->symbol_a,
	                                      test_data->symbol_b));
	assert(!nil_greater_than_or_equal_to_p(&nil_symbol_ordered,
	                                       test_data->symbol_a,
	                                       test_data->symbol_c));
	assert(nil_greater_than_or_equal_to_p(&nil_symbol_ordered,
	                                      test_data->symbol_c,
	                                      test_data->symbol_a));
}

void
setup(TestData *test_data)
{
	test_data->symbol_a = nil_symbol_make_static("hello");
	test_data->symbol_b = nil_symbol_make_static("hello");
	test_data->symbol_c = nil_symbol_make_static("world");
}
