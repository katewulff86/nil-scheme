/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <nil/test.h>
#include <nil/hash.h>
#include "hash-test.h"

static void     null_test(void *);
static void     byte_test(void *);
static void     hello_world_test(void *);
static void     string_test(void *);

static NIL_TEST_CASE(null_test_case, "null-test-case",
                     null_test, NULL, NULL);
static NIL_TEST_CASE(byte_test_case, "byte-test-case",
                     byte_test, NULL, NULL);
static NIL_TEST_CASE(hello_world_test_case, "hello-world-test-case",
                     hello_world_test, NULL, NULL);
static NIL_TEST_CASE(string_test_case, "string-test-case",
                     string_test, NULL, NULL);
NIL_TEST_SUITE(hash_suite, "hash-suite",
               &null_test_case,
               &byte_test_case,
               &hello_world_test_case,
               &string_test_case);

void
null_test(void *object)
{
	(void) object;
	assert(nil_murmur_hash(NULL, 0, 0) == 0);
}

void
byte_test(void *object)
{
	(void) object;
	uint64_t hashes[] = {
		0x1772864f16076936,
		0x0d463b74c07a9993,
		0x8cbfb00fde3c2acb,
		0x3c1bd7faf5df96bd,
		0x81990f77d6ebe3e6,
		0xea0d2f52cfbc979c,
		0xd06112bc50501164,
		0x9832e0b7ccc324ca,
		0xb5c073782913cba5,
		0x6f6e119cf87a0734,
		0x2762a9d61f6ceb3f,
		0xe96c8c4d8a52f93b,
		0xd230bd439fe3268a,
		0xf74a882a2f98faaf,
		0xc7d1d1b791baf9cc,
		0x4d6b100579200d96,
		0xf3266e438fe7652c,
		0xb6990d8351a37424,
		0xd673945faeee8ae2,
		0x00fe790117d92d62,
		0x9f9a6a002e4946c5,
		0x8c2eed77c673dd18,
		0xa8d85e061c66fe3a,
		0xacd85372c289f60a,
		0x1a59c27827306d91,
		0x1a2e96622b9a3eef,
		0x3dd724eb5237bdbe,
		0xe821dc0b3a8e72b0,
		0x3c99d84d4559108f,
		0xd6649f64e94a12df,
		0x5d4dfe2c45d21335,
		0x8e21f3a0a0a72c6b,
		0x2724cd8be2c78b81,
		0x225e477aafe88fa2,
		0xaedf34d85ba91867,
		0x89526369519cc2b9,
		0x898112064a22fb9f,
		0xbb4dbeeb9542ce97,
		0x6e2f43da25ffed74,
		0xe366830930467990,
		0xc04a3d3c0a1235a0,
		0x6f56e08f61a70c71,
		0x2f9d5bae743ad457,
		0xb1ebc10a6aa0db3d,
		0x8dffd3aa767e8981,
		0xf0b0893c4901bcf3,
		0x05d8e36501a0fc64,
		0x9617ab624d570557,
		0x1044375be9c1fd89,
		0xe5d15065738b8a46,
		0x1011297d4998a7d3,
		0xd18454b2d20aeb9a,
		0x2414c6a4f7f2b46e,
		0xd32788af927fec0e,
		0xce07a1edf6824583,
		0x889dd2851503acc1,
		0xde7a29e0162f0287,
		0xadcda5451e17a7b3,
		0x6da409caac26a5f5,
		0xadfb26c531b7a9e9,
		0xa49c1334f13d7b02,
		0xd1df68fb1bd07996,
		0x4f2056f71b0bd548,
		0x1dbc6a8e4aa1e926,
		0x91d47c8454499a52,
		0x3b22302bdcf4284d,
		0x58262013ef226102,
		0xfff445ee77bfe141,
		0xafce8fa67e305b7d,
		0x1ead7d1c000036ed,
		0xa89d3afebc8174a9,
		0x4f2751d8e2bb2da3,
		0xbd3e6c2b88b99602,
		0xd3eddd87d5dbb566,
		0x4d60dea40b3e42b3,
		0xcdb26530437fa75f,
		0xb068664c3e9d4150,
		0xac97c9225e13bf66,
		0x697b61c390703056,
		0x4776b59fa2f77b6c,
		0x81b42a68142cbbc0,
		0x6e1f20d7311d5eb4,
		0xa004a8f7e66a034c,
		0xcd624fc587f98bf5,
		0xb6b6a1ca2bad56e3,
		0x1d2c4263efef5e31,
		0x25910ad8a0616c87,
		0xb3c645d972ace3bb,
		0x30b4be99b0c277de,
		0x483484cda4c78c5d,
		0xa6bfc05cadbefe43,
		0x9fa96428d9dce7d1,
		0x6a00f8b7590a5d37,
		0x8332d79e9e0bf964,
		0x62f6e1a4010a315c,
		0x95eda186cfc53e61,
		0xfb55e9e37dbbca80,
		0x63e06f2da757f1d3,
		0x80b6ba49e59d4507,
		0xaf35d0f63e4801a3,
		0x6b4057bc5070ebab,
		0x935d303539038880,
		0x530e39150fb64b8e,
		0x412c062bd506a9b8,
		0xda7f942c594d9220,
		0x8ec731244b4ade4f,
		0xf91784c1e40ff408,
		0x274ca4c80432f643,
		0xd93112e3b84e61db,
		0xd71cc50182367fdd,
		0xcdb34ee73e1eba8c,
		0xfb4063978e593525,
		0x8e92ae85628a3148,
		0xc0efcbf37600cc47,
		0x94ca268563379aea,
		0xaca023f1e92586b1,
		0x7018103313dc94aa,
		0xa735096cf3258161,
		0x45daf0ce3526d141,
		0x7fc0efe27474fd17,
		0xbafb8597d1b4866b,
		0x82154294c3bb09ab,
		0x446c6e256a29121c,
		0xebdacdcd00b91cb1,
		0x82fdde47efed1f8a,
		0x4b1e11162e5e4cf2,
		0x2e9ee756a759f571,
		0xb5fbdd8ada289a14,
		0x65b50195ffc509ab,
		0x137404a1955fa477,
		0xdb489af80597ad32,
		0x7b5fd1997c5baf85,
		0x57e925860d36cc05,
		0xd58b458a58d2c0fd,
		0x94839354f5da9362,
		0x7e292dd562f96798,
		0x55893e310376b630,
		0xb2784a3573bbb9b4,
		0xc753a3c4bc1f95b6,
		0xf9c4cec99f6239ec,
		0x4ae3a0b89faef86a,
		0xe73136c10ac49c91,
		0x65d363cbc63cfdcf,
		0x77d29826e976e055,
		0xa66d27b8b849422b,
		0xc84db1b4d20834dc,
		0xb641bcae7b90cabe,
		0xaffa3f9b97475f9e,
		0x34ee8d6e92f1f326,
		0x40a807f660ff99cf,
		0xd68099f0fc745d35,
		0xa553368df4f9e45c,
		0xe073b201f5cc6245,
		0xb7210b3170f1a449,
		0x63c2712e05f5a31f,
		0xf2dd9c82634ba914,
		0xf66ac5ffee79ec73,
		0x917b08f8a7543168,
		0x805f21f1d38ae9f5,
		0xdb2894ffbaeb1054,
		0xcf0a858bb09b5b1a,
		0x7db1aa7acc942198,
		0x5614f51ce50c961d,
		0x1397236d7893e86b,
		0x215fc83416575a0a,
		0x726dff95ae165b0e,
		0x79153cbd93bab907,
		0xc474d68f9d67d76b,
		0x3d7ad0ddf060b39c,
		0xd18aad690f29af40,
		0x3ac98cc8a24584e6,
		0x895d652bf1e6be5c,
		0xf851ecc0ffcfb6cc,
		0xcbd8c786316ec39d,
		0xf589816535e60f7,
		0x7592b8aea3ea127c,
		0xc4095fe636379704,
		0x8186dc19bc2d189e,
		0xbf7d8dd4311bdb13,
		0x8a2d046ebee77845,
		0x8f575a0cbab06840,
		0x3ee30f282439de77,
		0x6bb39f904efa217b,
		0xf143252c64abd9bc,
		0x42b3ac73f4cb5b43,
		0x9689d6d22039633c,
		0x9e60efe177e54ed7,
		0xe31160b260fd50c,
		0x3b983b0e725bf061,
		0x91bec47ffc091353,
		0xd7c294d38745cc38,
		0x3d1aae420fa1e14a,
		0x87f3545405206ca9,
		0x4f4ba1b1585de295,
		0x5515bea59b006c75,
		0xbaf5d73ef93b1f11,
		0x7aed38c8737bc4c2,
		0xd59550226e65cdfc,
		0x3e0f3285403479c9,
		0xa7c045faf69fc0c,
		0xa11d2b1d56a29035,
		0x88ece54db1508b9e,
		0x48d002b59157a7fc,
		0xf0226fa48dfd480b,
		0x63f68463537fb75e,
		0x1cfac0837cdbff6c,
		0xf4e404a21bc1d388,
		0x3433a94d8f36ea5d,
		0x498868b30ceaa630,
		0xfa1b5c4b4f02fd72,
		0x721379b79b73d103,
		0xf981c6bb6ecfe232,
		0xb76fcb4968bb122e,
		0x4e3e297196f582e0,
		0x53d25feeb23e1c4a,
		0xd655dcf81826255a,
		0xf5765463bc84d668,
		0x40b35fa8b3dce63e,
		0x3e07dd951279590e,
		0xe3ace3b27d26aaf9,
		0x1bae91491d9481a,
		0xbdd7600058c60125,
		0xbbf6c1a3d42ff2df,
		0xedea55b31ea64ad9,
		0x82b626e5e5e76e5f,
		0x86a08b2080395713,
		0x152faecf39318402,
		0x84fe35567c1827f3,
		0xf6874a946e51dcd2,
		0x4bd625e1c198e26,
		0x4cbbbb5790cc6161,
		0xc9934e23f5db69c8,
		0x21db8c9dd138dd2b,
		0xe6df7fc6b23eb05e,
		0x3d4813bc0b25872a,
		0x7e153aa61395fda2,
		0x653c44f8c72ce396,
		0xd1a2d73b815a8dbc,
		0x88a5719ecbba3a6d,
		0x9792a9590cf3a671,
		0xbd9c3f3e4527a40a,
		0xf6b83047059853bc,
		0x7ce9e39c2d182997,
		0x51f5055a7c0b296f,
		0x930d695296a87e4f,
		0x80921da131906562,
		0x6ad3bce89427bdd1,
		0xa9e6f85a590573f5,
		0xcdb2e7277a1e8c31,
		0xa6616af5a3847dc7,
		0x8b5dcd25bc7234a5,
		0xe5bca73b6667dac1,
		0xc79f1317a3f978c3,
		0x90425f102328af39,
		0x7dc5b35235fd369f,
		0xbdf5206c9c625ec2,
	};
	for (size_t i = 0; i < sizeof(hashes)/sizeof(hashes[0]); ++i) {
		char input = i;
		uint64_t hash = hashes[i];
		assert(nil_murmur_hash(&input, sizeof(input), 0) ==
		       hash);
	}
}

void
hello_world_test(void *object)
{
	(void) object;
	char *hello = "Hello, World! This is a test.";
	assert(nil_murmur_hash(hello, 1, 0) == 0xbd3e6c2b88b99602);
	assert(nil_murmur_hash(hello, 2, 0) == 0x9023c08e2318602f);
	assert(nil_murmur_hash(hello, 3, 0) == 0xa684b41bc53481bd);
	assert(nil_murmur_hash(hello, 4, 0) == 0x9ef21c0208f3617a);
	assert(nil_murmur_hash(hello, 5, 0) == 0x95b99e307cc69158);
	assert(nil_murmur_hash(hello, 6, 0) == 0xc902c80c06e286f0);
	assert(nil_murmur_hash(hello, 7, 0) == 0xaab1db4dbbc3ccca);
	assert(nil_murmur_hash(hello, 8, 0) == 0x47f81be240712f89);
	assert(nil_murmur_hash(hello, 9, 0) == 0x0e4dd0059444fe18);
	assert(nil_murmur_hash(hello, 10, 0) == 0x611b951ac1b23f72);
	assert(nil_murmur_hash(hello, 11, 0) == 0x0047c509908a6389);
	assert(nil_murmur_hash(hello, 12, 0) == 0x2aa197b1c5f823b7);
	assert(nil_murmur_hash(hello, 13, 0) == 0x39ff4dff08c90f7f);
	assert(nil_murmur_hash(hello, 14, 0) == 0x675b0464c1b4b5e6);
	assert(nil_murmur_hash(hello, 15, 0) == 0x9eb8074d7584b541);
	assert(nil_murmur_hash(hello, 16, 0) == 0xbb11cd3a56ed7693);
	assert(nil_murmur_hash(hello, 17, 0) == 0xefad097c45f4fda3);
	assert(nil_murmur_hash(hello, 18, 0) == 0x71e150b3c5e13cab);
	assert(nil_murmur_hash(hello, 19, 0) == 0x106fde432631a814);
	assert(nil_murmur_hash(hello, 20, 0) == 0x8f06c8f26045e18e);
	assert(nil_murmur_hash(hello, 21, 0) == 0x101549544ee36387);
	assert(nil_murmur_hash(hello, 22, 0) == 0x37cdcdaa63bc9030);
	assert(nil_murmur_hash(hello, 23, 0) == 0x26223f7479d07ae8);
	assert(nil_murmur_hash(hello, 24, 0) == 0xb9966f9bf4753142);
	assert(nil_murmur_hash(hello, 25, 0) == 0x4d263aa146e968d1);
	assert(nil_murmur_hash(hello, 26, 0) == 0x8f293dc52c8e404c);
	assert(nil_murmur_hash(hello, 27, 0) == 0xfa17a255fb6e2ced);
	assert(nil_murmur_hash(hello, 28, 0) == 0xca200c9c1924b6a4);
	assert(nil_murmur_hash(hello, 29, 0) == 0x9c0e90213948c153);
}

void
string_test(void *object)
{
	(void) object;
	char const *strings[] = {
		"",
		"hello",
		"HELLO",
		"test",
		"abc123",
		"this is quite a long string",
		"and yet this is an even longer string",
	};
	size_t num_strings = sizeof(strings)/sizeof(strings[0]);
	for (size_t i = 0; i < num_strings; ++i) {
		char const *string = strings[i];
		size_t length = strlen(string);
		assert(nil_murmur_hash(string, length, 0) ==
		       nil_murmur_hash_string(string, 0));
	}
}
