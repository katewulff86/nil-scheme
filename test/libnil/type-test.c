/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include "type/bool-test.h"
#include "type/bytevector-test.h"
#include "type/char-test.h"
#include "type/eof-object-test.h"
#include "type/function-test.h"
#include "type/hash-map-test.h"
#include "type/list-test.h"
#include "type/null-test.h"
#include "type/pair-test.h"
#include "type/string-test.h"
#include "type/symbol-test.h"
#include "type/thunk-test.h"

NIL_TEST_SUITE(type_suite, "type-suite",
               &type_bool_suite,
               &type_bytevector_suite,
               &type_char_suite,
               &type_eof_object_suite,
               &type_function_suite,
               &type_hash_map_suite,
               &type_list_suite,
               &type_null_suite,
               &type_pair_suite,
               &type_string_suite,
               &type_symbol_suite,
               &type_thunk_suite);
