/* Copyright 2020 Katharina Wulff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include <nil/test.h>
#include "hash-test.h"
#include "object-test.h"
#include "type-test.h"
#include "class-test.h"

NIL_TEST_SUITE(libnil_test_suite, "libnil-test-suite",
               &hash_suite,
               &object_suite,
               &type_suite,
               &class_suite);

int
main()
{
	NilTestSuiteResult *result =
	    nil_test_suite_run(&libnil_test_suite);
	return nil_test_suite_result_report(result);
}
