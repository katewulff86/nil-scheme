LIBNIL_CHECK_SOURCES =                                          \
	test/libnil/class/equal/symbol-equal-test.c             \
	test/libnil/class/ordered/symbol-ordered-test.c         \
	test/libnil/class/equal-test.c                          \
	test/libnil/class/ordered-test.c                        \
	test/libnil/type/bool-test.c                            \
	test/libnil/type/bytevector-test.c                      \
	test/libnil/type/char-test.c                            \
	test/libnil/type/environment-test.c                     \
	test/libnil/type/eof-object-test.c                      \
	test/libnil/type/function-test.c                        \
	test/libnil/type/hash-map-test.c                        \
	test/libnil/type/list-test.c                            \
	test/libnil/type/null-test.c                            \
	test/libnil/type/pair-test.c                            \
	test/libnil/type/string-test.c                          \
	test/libnil/type/symbol-test.c                          \
	test/libnil/type/thunk-test.c                           \
	test/libnil/check.c                                     \
	test/libnil/class-test.c                                \
	test/libnil/hash-test.c                                 \
	test/libnil/object-test.c                               \
	test/libnil/type-test.c

LIBNIL_CHECK_OBJS = $(LIBNIL_CHECK_SOURCES:%.c=%.o)
LIBNIL_CHECK_DEPS = $(LIBNIL_CHECK_SOURCES:%.c=deps/%.d)

test/libnil-check: $(LIBNIL_CHECK_OBJS) libnil.a libnil-test.a
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(LIBNIL_CHECK_OBJS)   \
	    libnil.a libnil-test.a

include $(LIBNIL_CHECK_DEPS)
